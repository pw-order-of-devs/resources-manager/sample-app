FROM node:18-alpine as builder

WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH

COPY package.json ./
COPY yarn.lock ./
RUN yarn --silent

COPY . ./
RUN yarn build

FROM nginx:1.21-alpine as runner

RUN apk add --no-cache bash curl

WORKDIR /app
COPY --from=builder /app/build /usr/share/nginx/html/sample-app
COPY ./nginx/default.conf /etc/nginx/conf.d/
COPY ./scripts ./scripts
COPY ./entrypoint.sh ./

CMD ["./entrypoint.sh"]
