#!/bin/bash

BASE_PATH="/usr/share/nginx/html/sample-app"
ENV_FILE="${BASE_PATH}/env.js"

rm "$ENV_FILE"
touch "$ENV_FILE"
echo "window._env_ = {" >> "$ENV_FILE"

for var in "${!REACT_APP_@}"; do
  echo "  $var: \"${!var}\"," >> "$ENV_FILE"
done

echo "}" >> "$ENV_FILE"
