#!/bin/bash

CONSUL_ENABLED=${CONSUL_ENABLED-false}
CONSUL_HOST=${CONSUL_HOST-consul}
APP_NAME="sample-app"

if [ "$CONSUL_ENABLED" = "true" ]; then
  echo "cleaning up ..."
  curl -X PUT \
   "http://${CONSUL_HOST}:8500/v1/agent/service/deregister/${APP_NAME}"
fi
