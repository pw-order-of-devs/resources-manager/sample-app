#!/bin/bash

CONSUL_ENABLED=${CONSUL_ENABLED-false}
CONSUL_HOST=${CONSUL_HOST-consul}

VERSION=${VERSION-"0.1.0"}
APP_NAME="sample-app"
PORT=${PORT-3000}

REGISTER_PAYLOAD="{
  \"ID\": \"${APP_NAME}\",
  \"Name\": \"${APP_NAME}\",
  \"Address\": \"${APP_NAME}\",
  \"Port\": ${PORT},
  \"Tags\": [
   \"primary\",
   \"urlprefix-/sample-app\",
   \"resources-manager\",
   \"front\"
  ],
  \"EnableTagOverride\": false,
  \"META\": {
   \"version\": \"${VERSION}\"
  },
  \"Check\": {
   \"Name\": \"Resources Manager Sample App HTTP Check\",
   \"HTTP\": \"http://${APP_NAME}:${PORT}/health\",
   \"Interval\": \"10s\",
   \"Timeout\": \"2s\"
  },
  \"Weights\": {
   \"Passing\": 10,
   \"Warning\": 1
  }
}"

if [ "$CONSUL_ENABLED" = "true" ]; then
  echo "trying to register in consul ..."
  response=$(
    curl -s -o /dev/null -w "%{http_code}" -X PUT \
     -H "Content-Type: application-json" \
     -d "$REGISTER_PAYLOAD" \
     "http://${CONSUL_HOST}:8500/v1/agent/service/register"
  )
  if [ "$response" -ne "200" ]; then
    echo "failed registering in consul - exiting ..."
    exit 1
  fi
fi
