build:

	docker-compose -f docker-compose-dev.yml build sample-app

lint:

	yarn lint

up:

	docker-compose -f docker-compose-dev.yml up -d sample-app

down:

	docker-compose -f docker-compose-dev.yml down

logs:

	docker-compose -f docker-compose-dev.yml logs -f sample-app
