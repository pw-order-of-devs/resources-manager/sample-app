# Sample App

Sample application for Resources Manager written with React.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn test`

Launches the test runner in the interactive watch mode.

### `yarn build`

Builds the app for production to the `build` folder.

### `yarn lint`

Runs the linter for the project.

### `yarn lint:fix`

Runs the linter for the project and automatically fixes the problems.
