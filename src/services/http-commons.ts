import axios from 'axios'
import { getEnvVar } from 'utils/env'

const API_HOST = getEnvVar('RESOURCES_API_URL')
const API_BASE_URL = API_HOST + '/api/v1'

const api = axios.create({
  baseURL: API_BASE_URL,
  headers: { 'Content-type': 'application/json' },
})

export default api
