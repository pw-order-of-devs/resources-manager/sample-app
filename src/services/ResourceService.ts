import { PaginationData } from 'model/Pagination'
import { FileMetadata, PaginatedResource, Resource } from 'model/Resource'
import http from 'services/http-commons'

interface ResourceService {
  getResources: (pagination: PaginationData) => Promise<PaginatedResource>
  getResourceById: (id: string) => Promise<Resource>
  uploadResourceContent: (id: string, content: string) => Promise<number>
  createResource: (resource: FileMetadata) => Promise<string>
  updateResource: (id: string, resource: FileMetadata) => Promise<number>
  deleteResource: (id: string) => Promise<number>
}

const RESOURCES_API = 'resources'

const service: ResourceService = {
  getResources(pagination: PaginationData) {
    return http
      .get(RESOURCES_API + '?pageSize=' + pagination.PageSize + '&pageNumber=' + pagination.PageNumber)
      .then((data) => data.data)
  },
  getResourceById(id: string) {
    return http.get(RESOURCES_API + '/' + id).then((data) => data.data)
  },
  uploadResourceContent(id: string, content: string) {
    return http
      .put(RESOURCES_API + '/' + id + '/upload', content, { headers: { 'Content-Type': 'application/octet-stream' } })
      .then((data) => data.data)
  },
  createResource(resource: FileMetadata) {
    return http
      .post(RESOURCES_API, resource, { headers: { 'Content-Type': 'application/json' } })
      .then((data) => data.data)
  },
  updateResource(id: string, resource: FileMetadata) {
    return http
      .put(RESOURCES_API + '/' + id, resource, { headers: { 'Content-Type': 'application/json' } })
      .then((data) => data.data)
  },
  deleteResource(id: string) {
    return http.delete(RESOURCES_API + '/' + id).then((data) => data.data)
  },
}

export default service
