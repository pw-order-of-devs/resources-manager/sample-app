const ResourcesListHeader = () => (
  <thead>
    <tr>
      <th>Name</th>
      <th>Type</th>
      <th>Created at</th>
      <th>Last modified at</th>
      <th>Tags</th>
      <th />
    </tr>
  </thead>
)

export default ResourcesListHeader
