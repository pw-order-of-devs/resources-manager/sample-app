import { YesNoDialog } from 'components/common/Modals'
import { Resource } from 'model/Resource'
import { useState } from 'react'
import { useAlert } from 'react-alert'
import { useNavigate } from 'react-router-dom'
import service from 'services/ResourceService'

interface ResourcesListBodyProperties {
  resources: Resource[]
  onDelete: () => void
}

const ResourcesListBody = (props: ResourcesListBodyProperties) => {
  const alert = useAlert()
  const navigate = useNavigate()
  const [showDeleteModal, setShowDeleteModal] = useState(false)

  const deleteResource = (id: string) =>
    service
      .deleteResource(id)
      .then(() => setShowDeleteModal(false))
      .then(() => props.onDelete())
      .then(() => alert.success('Resource was deleted successfully'))
      .catch((error) => alert.error(error.message))

  return (
    <tbody>
      {props.resources.map((resource) => (
        <tr key={resource.Id + Math.random()}>
          <td className="align-middle">{resource.Metadata.Name}</td>
          <td className="align-middle">{resource.Metadata.FileType}</td>
          <td className="align-middle">{new Date(resource.CreationDate).toUTCString()}</td>
          <td className="align-middle">
            {!resource.ModificationDate ? '-' : new Date(resource.ModificationDate).toUTCString()}
          </td>
          <td className="align-middle">{resource.Metadata.TagList}</td>
          <td>
            <button
              onClick={() => navigate('details', { state: { id: resource.Id, mode: 'view' } })}
              className="btn btn-info"
            >
              Details
            </button>
            <button
              onClick={() => navigate('details', { state: { id: resource.Id, mode: 'edit' } })}
              className="btn btn-warning"
            >
              Edit
            </button>
            <button onClick={() => setShowDeleteModal(true)} className="btn btn-danger">
              Delete
            </button>
          </td>
          <YesNoDialog
            show={showDeleteModal}
            label="Are you sure to delete this item?"
            handleHide={() => setShowDeleteModal(false)}
            callback={() => deleteResource(resource.Id)}
          />
        </tr>
      ))}
    </tbody>
  )
}

export default ResourcesListBody
