import React, { MouseEventHandler, ReactNode } from 'react'
import { AlertOptions, positions, transitions } from 'react-alert'

interface template {
  style: React.CSSProperties | undefined
  options: AlertOptions
  message: ReactNode | undefined
  close: MouseEventHandler<HTMLButtonElement>
}

export const CustomAlertOptions = {
  position: positions.TOP_RIGHT,
  timeout: 3000,
  offset: '0.5em',
  transition: transitions.FADE,
}

export const AlertTemplate = (t: template) => (
  <div style={t.style} className={'sample-app-alert alert-' + t.options.type}>
    <span>{t.message}</span>
    <button onClick={t.close} className={'sample-app-alert-button'}>
      X
    </button>
  </div>
)
