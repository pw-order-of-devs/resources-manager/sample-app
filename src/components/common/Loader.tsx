import 'styles/loader.css'

const getDefaultStyle = (visible: boolean) => ({
  display: visible ? 'flex' : 'none',
})

interface LoaderProps {
  visible: boolean
}

const svgCircle = (cx: string, cy: string, begin: string) => (
  <circle cx={cx} cy={cy} r="10">
    <animate
      attributeName="fill-opacity"
      begin={begin}
      dur="1s"
      values="1;.2;1"
      calcMode="linear"
      repeatCount="indefinite"
    />
  </circle>
)

const svgContent = () => (
  <svg width="150" height="150" viewBox="0 0 105 105" fill="black">
    {svgCircle('12.5', '12.5', '0s')}
    {svgCircle('12.5', '52.5', '100ms')}
    {svgCircle('52.5', '12.5', '300ms')}
    {svgCircle('52.5', '52.5', '600ms')}
    {svgCircle('92.5', '12.5', '800ms')}
    {svgCircle('92.5', '52.5', '400ms')}
    {svgCircle('12.5', '92.5', '700ms')}
    {svgCircle('52.5', '92.5', '500ms')}
    {svgCircle('92.5', '92.5', '200ms')}
  </svg>
)

const Loader = (props: LoaderProps) => (
  <div style={{ ...getDefaultStyle(props.visible) }} className={'loader-wrapper'}>
    {svgContent()}
  </div>
)

export default Loader
