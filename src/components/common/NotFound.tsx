import logo from 'logo.svg'

const NotFound = () => {
  return (
    <div className="app-dark">
      <img src={logo} className="app-dark-logo" alt="logo" />
      <div className="page-error-label fixed-bottom">err: page not found</div>
    </div>
  )
}

export default NotFound
