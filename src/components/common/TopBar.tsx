import logo from 'logo.svg'
import { useNavigate } from 'react-router-dom'

interface TopBarProperties {
  withCreateButton?: boolean
}

const TopBar = (props: TopBarProperties) => {
  const navigate = useNavigate()
  return (
    <nav id="sample-app-top-bar" className="navbar navbar-expand-md navbar-dark bg-dark mb-4 px-4 fixed-top">
      <img src={logo} className="app-logo" alt="logo" />
      <div className="navbar-brand">Resources Manager - Sample App</div>
      <div className="mx-auto" />
      {props.withCreateButton && (
        <button className="btn btn-primary" onClick={() => navigate('details', { state: { id: '', mode: 'create' } })}>
          Upload new resource
        </button>
      )}
    </nav>
  )
}

export default TopBar
