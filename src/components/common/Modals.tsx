import { MouseEventHandler } from 'react'
import { Button, Modal } from 'react-bootstrap'

interface properties {
  label: string
  show: boolean
  handleHide: () => void
  callback: MouseEventHandler<HTMLButtonElement>
}

export const YesNoDialog = (p: properties) => (
  <Modal show={p.show} backdrop="static" keyboard={false} onHide={p.handleHide}>
    <Modal.Header closeButton>
      <Modal.Title>{p.label}</Modal.Title>
    </Modal.Header>
    <Modal.Footer>
      <Button variant="secondary" onClick={p.handleHide}>
        No
      </Button>
      <Button variant="primary" onClick={p.callback}>
        Yes
      </Button>
    </Modal.Footer>
  </Modal>
)
