import logo from 'logo.svg'
import { ReactNode, useEffect, useState } from 'react'

const MobilePage = () => (
  <div className="app-dark">
    <img src={logo} className="app-dark-logo" alt="logo" />
  </div>
)

interface PageProperties {
  children: ReactNode
}

const DesktopOnlyPage = (props: PageProperties) => {
  const [width, setWidth] = useState<number>(window.innerWidth)
  const handleWindowSizeChange = () => setWidth(window.innerWidth)
  useEffect(() => {
    window.addEventListener('resize', handleWindowSizeChange)
    return () => window.removeEventListener('resize', handleWindowSizeChange)
  }, [])

  const isMobile = width <= 768

  return (
    <div>
      {!isMobile && props.children}
      {isMobile && <MobilePage />}
    </div>
  )
}

export default DesktopOnlyPage
