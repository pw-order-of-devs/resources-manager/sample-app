import Loader from 'components/common/Loader'
import TopBar from 'components/common/TopBar'
import { ReactNode } from 'react'

interface PageProperties {
  children: ReactNode
  loading: boolean
  withCreateButton?: boolean
}

const BasePage = (props: PageProperties) => (
  <div className="row px-5">
    <TopBar withCreateButton={props.withCreateButton} />
    <Loader visible={props.loading} />
    {props.children}
  </div>
)

export default BasePage
