import { FileType, Resource, TimePinpoint, defaultResource } from 'model/Resource'
import BasePage from 'pages/BasePage'
import React, { ChangeEvent, useEffect, useState } from 'react'
import { useAlert } from 'react-alert'
import { Button, Form, FormControl, InputGroup } from 'react-bootstrap'
import { BsFillCaretDownFill } from 'react-icons/bs'
import { useLocation, useNavigate } from 'react-router-dom'
import service from 'services/ResourceService'
import 'styles/resources.css'

type ViewEditMode = 'view' | 'edit' | 'create'

const supportedFileTypes = {
  text: 'text/*,application/pdf',
  audio: 'audio/*',
  video: 'video/*',
}

interface stateParameters {
  id: string
  mode: ViewEditMode
}

const parseDate = (date: number | undefined) => {
  if (!date) return ''
  return new Intl.DateTimeFormat('en-US', {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
  }).format(new Date(date))
}

const parseTagList = (tagList: string[] | undefined) => {
  if (!tagList) return ''
  return JSON.stringify(tagList)
}

const parseTimePinpoints = (pinpoints: TimePinpoint[] | undefined) => {
  if (!pinpoints) return ''
  return JSON.stringify(pinpoints)
}

const formatBytes = (bytes?: number) => {
  if (!bytes || bytes === 0) return '0 Bytes'
  const k = 1024,
    sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
  const i = Math.floor(Math.log(bytes) / Math.log(k))
  return parseFloat((bytes / Math.pow(k, i)).toFixed(3)) + ' ' + sizes[i]
}

const formatDuration = (seconds?: number) => {
  if (!seconds || seconds === 0) return 'none'
  const time = { year: 31536000, day: 86400, hour: 3600, minute: 60, second: 1 },
    res: string[] = []

  Object.entries(time).forEach(([key, value]) => {
    if (seconds && seconds >= value) {
      const val = Math.floor(seconds / value)
      res.push(val.toString() + (val > 1 ? ' ' + key + 's' : ' ' + key))
      seconds /= val
    }
  })
  return res.length > 1 ? res.join(', ').replace(/,([^,]*)$/, ' and $1') : res[0]
}

const ResourceDetails = () => {
  const alert = useAlert()
  const location = useLocation()
  const navigate = useNavigate()

  const [loading, setLoading] = useState<boolean>(false)
  const [resource, setResource] = useState<Resource | undefined>(undefined)
  const [resourceFileContent, setResourceFileContent] = useState<string | undefined>(undefined)
  const [isVisibleCondition, setIsVisibleCondition] = useState<boolean>(false)
  const [fileType, setFileType] = useState<string>(supportedFileTypes.text)
  const [fileSizeView, setFileSizeView] = useState<string>('')
  const [fileDurationView, setFileDurationView] = useState<string>('')
  const [mode, setMode] = useState<string>('view')

  const state = location.state as stateParameters
  const fileNameRef = React.createRef<HTMLInputElement>(),
    filePickerRef = React.createRef<HTMLInputElement>()

  useEffect(() => {
    setMode(state.mode)
    if (state.mode !== 'create') {
      setLoading(true)
      service
        .getResourceById(state.id)
        .then((data) => {
          setResource(data)
          setFileType(data.Metadata.FileType)
          setFileSizeView(formatBytes(data.Metadata.Size))
          setFileDurationView(formatDuration(data.Metadata.Duration))
        })
        .catch((error) => alert.error(error.message))
        .finally(() => setLoading(false))
    } else {
      setResource(defaultResource())
    }
  }, [])

  const chooseFile = () => {
    filePickerRef.current?.click()
  }

  const getFileDuration = async (file: File) =>
    new Promise((resolve, reject) => {
      if (resource) {
        let node: HTMLAudioElement | HTMLVideoElement
        if (resource.Metadata.FileType === 'audio') node = document.createElement('audio')
        else if (resource.Metadata.FileType === 'video') node = document.createElement('video')
        else return resolve('text file')
        node.preload = 'metadata'
        node.onloadedmetadata = () => {
          resource.Metadata.Duration = node.duration
          setResource(resource)
          setFileDurationView(formatDuration(node.duration))
          window.URL.revokeObjectURL(node.src)
          resolve(Promise.resolve())
        }
        node.src = URL.createObjectURL(file)
      } else reject('no file')
    })

  const getFileMetadata = async (file: File) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader()
      reader.onload = (ev) => {
        if (resource && ev.target) {
          setResourceFileContent(ev.target.result as string)
          resource.Metadata.Name = file.name
          resource.Metadata.Size = file.size
          setResource(resource)
          setFileSizeView(formatBytes(file.size))
          if (fileNameRef.current) fileNameRef.current.value = file.name
          resolve(getFileDuration(file))
        } else reject('no file')
      }
      reader.readAsDataURL(file)
    })

  const fileUploaded = (e: ChangeEvent<HTMLInputElement>) => {
    if (resource && e.target.files?.length === 1) {
      setLoading(true)
      getFileMetadata(e.target.files[0])
        .catch((err) => alert.error(err))
        .finally(() => setLoading(false))
    }
  }

  const nameChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (resource) resource.Metadata.Name = e.currentTarget.value
  }

  const descriptionChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
    if (resource) resource.Metadata.Description = e.currentTarget.value
  }

  const fileTypeChange = (e: ChangeEvent<HTMLSelectElement>) => {
    const newFileType = e.currentTarget.value as FileType
    if (resource) {
      resource.Metadata.FileType = newFileType
      setResource(resource)
    }
    setIsVisibleCondition(newFileType !== 'text')
    setFileType(supportedFileTypes[newFileType])
  }

  const tagListChange = (e: ChangeEvent<HTMLInputElement>) => {
    try {
      if (resource) resource.Metadata.TagList = JSON.parse(e.currentTarget.value)
    } catch (e) {
      return
    }
  }

  const timePinpointsChange = (e: ChangeEvent<HTMLInputElement>) => {
    try {
      if (resource) resource.Metadata.TimePinpoints = JSON.parse(e.currentTarget.value)
    } catch (e) {
      return
    }
  }

  const saveResourceContent = (content: string) => {
    service
        .uploadResourceContent(state.id, content)
        .then(() => alert.success('Successfully updated file content'))
        .catch((error) => alert.error(error.message))
        .finally(() => setLoading(false))
  }

  const saveResource = () => {
    if (resource) {
      setLoading(true)
      let response
      if (mode === 'edit') response = service.updateResource(state.id, resource.Metadata)
      else if (mode === 'create') response = service.createResource(resource.Metadata)
      else return
      response
        .then(() => alert.success('Successfully saved entry'))
        .then(() => setMode('view'))
        .then(() => { if (resourceFileContent) saveResourceContent(resourceFileContent) })
        .catch((error) => alert.error(error.message))
        .finally(() => setLoading(false))
    }
  }

  return (
    <BasePage loading={loading}>
      <Form className="resources-details-form">
        <InputGroup>
          <InputGroup.Text>File name</InputGroup.Text>
          <FormControl
            ref={fileNameRef}
            defaultValue={resource?.Metadata.Name || ''}
            onChange={nameChange}
            disabled={mode === 'view'}
          />
          {mode !== 'view' && (
            <input
              type="file"
              accept={fileType}
              style={{ display: 'none' }}
              ref={filePickerRef}
              onChange={fileUploaded}
            />
          )}
          {mode !== 'view' && (
            <Button variant="outline-secondary" onClick={chooseFile}>
              Upload
            </Button>
          )}
        </InputGroup>
        <InputGroup>
          <InputGroup.Text>Description</InputGroup.Text>
          <textarea
            className="form-control"
            defaultValue={resource?.Metadata.Description || ''}
            onChange={descriptionChange}
            disabled={mode === 'view'}
          />
        </InputGroup>
        <InputGroup>
          <InputGroup.Text>File type</InputGroup.Text>
          <BsFillCaretDownFill className="select-box-caret-icon" />
          <select
            className="form-control"
            value={resource?.Metadata.FileType || 'text'}
            onChange={fileTypeChange}
            disabled={mode === 'view'}
          >
            <option>text</option>
            <option>audio</option>
            <option>video</option>
          </select>
        </InputGroup>
        <InputGroup>
          <InputGroup.Text>Author</InputGroup.Text>
          <FormControl value={resource?.Metadata.AuthorId || ''} disabled />
        </InputGroup>
        <InputGroup>
          <InputGroup.Text>Created at</InputGroup.Text>
          <FormControl value={parseDate(resource?.CreationDate) || ''} disabled />
        </InputGroup>
        <InputGroup>
          <InputGroup.Text>Last modified at</InputGroup.Text>
          <FormControl value={parseDate(resource?.ModificationDate) || ''} disabled />
        </InputGroup>
        <InputGroup>
          <InputGroup.Text>Size</InputGroup.Text>
          <FormControl value={fileSizeView || ''} disabled />
        </InputGroup>
        {isVisibleCondition && (
          <InputGroup>
            <InputGroup.Text>Duration</InputGroup.Text>
            <FormControl value={fileDurationView || ''} disabled />
          </InputGroup>
        )}
        <InputGroup>
          <InputGroup.Text>Tag list</InputGroup.Text>
          <FormControl
            defaultValue={parseTagList(resource?.Metadata.TagList) || ''}
            onChange={tagListChange}
            disabled={mode === 'view'}
          />
        </InputGroup>
        <InputGroup>
          <InputGroup.Text>Time pinpoints</InputGroup.Text>
          <FormControl
            defaultValue={parseTimePinpoints(resource?.Metadata.TimePinpoints) || ''}
            onChange={timePinpointsChange}
            disabled={mode === 'view'}
          />
        </InputGroup>
      </Form>
      {mode !== 'view' && (
        <Button className="form-submit-button mx-3" variant="primary" onClick={saveResource}>
          Submit
        </Button>
      )}
      {mode === 'view' && (
        <Button className="form-submit-button mx-3" variant="warning" onClick={() => setMode('edit')}>
          Edit
        </Button>
      )}
      <Button className="form-submit-button mx-3" variant="danger" onClick={() => navigate('/sample-app')}>
        Close
      </Button>
    </BasePage>
  )
}

export default ResourceDetails
