import ResourcesListBody from 'components/resources/ResourcesListBody'
import ResourcesListHeader from 'components/resources/ResourcesListHeader'
import { PaginationData, PaginationDefault } from 'model/Pagination'
import { Resource } from 'model/Resource'
import BasePage from 'pages/BasePage'
import { FunctionComponent, useEffect, useState } from 'react'
import { useAlert } from 'react-alert'
import Pagination from 'react-bootstrap/Pagination'
import service from 'services/ResourceService'
import 'styles/resources.css'

const ResourcesList: FunctionComponent = () => {
  const alert = useAlert()

  const [loading, setLoading] = useState<boolean>(false)
  const [resources, setResources] = useState<Resource[]>([])
  const [page, setPage] = useState<PaginationData>(PaginationDefault)
  const [paginationItems, setPaginationItems] = useState<number[]>([])
  const [refreshKey, setRefreshKey] = useState(0)

  useEffect(() => {
    setLoading(true)
    service
      .getResources(page)
      .then((response) => {
        setResources(response.Resources === null ? [] : response.Resources)
        setPage(response.Pagination)
      })
      .catch((error) => alert.error(error.message))
      .finally(() => setLoading(false))
  }, [refreshKey])

  useEffect(() => {
    const paginationItems = [...Array(Math.ceil(page.Total / page.PageSize)).keys()].map((_, i) => i + 1)
    setPaginationItems(paginationItems)
  }, [page])

  const paginationItemClick = (i: number) => {
    if (page.PageNumber !== i && 0 <= i && i <= paginationItems.length) {
      page.PageNumber = i
      setPage(page)
      setRefreshKey(refreshKey + 1)
    }
  }

  return (
    <BasePage loading={loading} withCreateButton>
      <table className="resources-list-table table table-striped table-bordered">
        <ResourcesListHeader />
        <ResourcesListBody resources={resources} onDelete={() => setRefreshKey(refreshKey + 1)} />
      </table>
      <div className="row">
        <Pagination className="justify-content-center">
          <Pagination.First onClick={() => paginationItemClick(0)} />
          <Pagination.Prev onClick={() => paginationItemClick(page.PageNumber - 1)} />
          {paginationItems.map((item) => (
            <Pagination.Item key={item} active={item === page.PageNumber} onClick={() => paginationItemClick(item)}>
              {item}
            </Pagination.Item>
          ))}
          <Pagination.Next onClick={() => paginationItemClick(page.PageNumber + 1)} />
          <Pagination.Last onClick={() => paginationItemClick(paginationItems.length)} />
        </Pagination>
      </div>
    </BasePage>
  )
}

export default ResourcesList
