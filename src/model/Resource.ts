import { PaginationData } from 'model/Pagination'

export type FileType = 'text' | 'audio' | 'video'

export interface TimePinpoint {
  Moment: number
  Description: string
}

export interface FileMetadata {
  FileType: FileType
  Name: string
  Description: string | undefined
  AuthorId: string | undefined
  Size: number | undefined
  Duration: number | undefined
  TimePinpoints: TimePinpoint[]
  TagList: string[]
}

export interface Resource {
  Id: string
  CreationDate: number
  ModificationDate?: number
  Metadata: FileMetadata
}

export interface PaginatedResource {
  Resources: Resource[]
  Pagination: PaginationData
}

export const defaultResource = () => {
  return {
    Id: '00000000-0000-0000-0000-00000000',
    CreationDate: 0,
    ModificationDate: 0,
    Metadata: {
      Name: '',
      Description: undefined,
      FileType: 'text' as FileType,
      AuthorId: undefined,
      Size: undefined,
      Duration: undefined,
      TagList: [],
      TimePinpoints: [],
    },
  }
}
