export interface PaginationData {
  PageNumber: number
  PageSize: number
  Total: number
}

export const PaginationDefault = (): PaginationData => {
  return { PageNumber: 1, PageSize: 10, Total: 0 }
}
