import 'bootstrap/dist/css/bootstrap.min.css'
import { AlertTemplate, CustomAlertOptions } from 'components/common/Alerts'
import DesktopOnlyPage from 'components/common/DesktopOnly'
import NotFound from 'components/common/NotFound'
import ResourceDetails from 'pages/ResourceDetails'
import ResourcesList from 'pages/ResourcesList'
import React from 'react'
import { Provider as AlertProvider } from 'react-alert'
import ReactDOM from 'react-dom/client'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import 'styles/alerts.css'
import 'styles/app.css'
import 'styles/index.css'

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <DesktopOnlyPage>
      <AlertProvider template={AlertTemplate} containerStyle={{ marginTop: '5em' }} {...CustomAlertOptions}>
        <BrowserRouter>
          <Routes>
            <Route path="sample-app">
              <Route index element={<ResourcesList />} />
              <Route path="details" element={<ResourceDetails />} />
            </Route>
            <Route path="*" element={<NotFound />} />
          </Routes>
        </BrowserRouter>
      </AlertProvider>
    </DesktopOnlyPage>
  </React.StrictMode>,
)
