declare global {
  interface EnvVars {
    REACT_APP_RESOURCES_API_URL: string
  }

  interface Window {
    _env_: EnvVars
  }
}

window._env_ = window._env_ || {}

export const getEnvVar = (key: string) => {
  switch (key) {
    case 'RESOURCES_API_URL':
      return window._env_.REACT_APP_RESOURCES_API_URL
    default:
      return undefined
  }
}
