#!/bin/sh

scripts/prepare_env.sh
scripts/consul_register.sh
echo "starting sample app service ..."
sed  -i "s/80;/${PORT-3000};/g" /etc/nginx/conf.d/default.conf
nginx -g "daemon off;"
scripts/consul_deregister.sh
